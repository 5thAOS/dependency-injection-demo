package com.example.rany.dependencyinjectiondemo.di.module;

import com.example.rany.dependencyinjectiondemo.api.ArticleService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module(includes = NetworkModule.class)
public class ResfulModule {

    public static final String BASE_URL = "http://api-ams.me/v1/api/";

    @Provides
    @Singleton
    public Retrofit provideRetrofit(OkHttpClient okHttpClient){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit;
    }

    @Provides
    @Singleton
    public ArticleService getArticleService(OkHttpClient okHttpClient){
         return provideRetrofit(okHttpClient).create(ArticleService.class);
    }




}
