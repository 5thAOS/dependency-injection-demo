package com.example.rany.dependencyinjectiondemo.di.component;

import com.example.rany.dependencyinjectiondemo.MainActivity;
import com.example.rany.dependencyinjectiondemo.api.ArticleService;
import com.example.rany.dependencyinjectiondemo.di.module.MyAppModule;
import com.example.rany.dependencyinjectiondemo.di.module.NetworkModule;
import com.example.rany.dependencyinjectiondemo.di.module.ResfulModule;
import com.example.rany.dependencyinjectiondemo.share_preference.MySharePreference;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {MyAppModule.class, NetworkModule.class, ResfulModule.class})
public interface MyAppComponent {

    void inject(MainActivity mainActivity);
    MySharePreference getSharePreference();

}
