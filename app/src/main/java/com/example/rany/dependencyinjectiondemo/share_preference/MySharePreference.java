package com.example.rany.dependencyinjectiondemo.share_preference;

import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Inject;

public class MySharePreference implements MySharePreAccesser {

    public static final String MY_PREF = "my_prefe";
    public static final String ARTICLE_PREF = "article_pref";

    private SharedPreferences preferences;

    @Inject
    public MySharePreference(Context context){
        preferences = context.getSharedPreferences(MY_PREF, Context.MODE_PRIVATE);
    }

    @Override
    public void setPreference(String title) {
        preferences.edit().putString(ARTICLE_PREF, title).apply();
    }

    @Override
    public String getPreference() {
        return preferences.getString(ARTICLE_PREF, "n/a");
    }
}
