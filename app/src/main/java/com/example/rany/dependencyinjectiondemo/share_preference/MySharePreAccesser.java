package com.example.rany.dependencyinjectiondemo.share_preference;

public interface MySharePreAccesser {

    void setPreference(String title);
    String getPreference();

}
