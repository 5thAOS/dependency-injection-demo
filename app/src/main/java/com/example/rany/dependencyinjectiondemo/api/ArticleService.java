package com.example.rany.dependencyinjectiondemo.api;

import com.example.rany.dependencyinjectiondemo.response.ArticleResponse;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ArticleService {

    @GET("articles")
    Call<ArticleResponse> getAllArticle();

}
