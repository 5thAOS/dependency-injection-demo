package com.example.rany.dependencyinjectiondemo.di;

import android.app.Application;
import android.content.Context;

import com.example.rany.dependencyinjectiondemo.di.component.DaggerMyAppComponent;
import com.example.rany.dependencyinjectiondemo.di.component.MyAppComponent;
import com.example.rany.dependencyinjectiondemo.di.module.MyAppModule;

public class MyApp extends Application {

    private MyAppComponent myAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        myAppComponent = DaggerMyAppComponent.builder()
                .myAppModule(new MyAppModule(this))
                .build();
    }

    public MyAppComponent getMyAppComponent() {
        return myAppComponent;
    }
}
