package com.example.rany.dependencyinjectiondemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.example.rany.dependencyinjectiondemo.api.ArticleService;
import com.example.rany.dependencyinjectiondemo.di.MyApp;
import com.example.rany.dependencyinjectiondemo.response.ArticleResponse;
import com.example.rany.dependencyinjectiondemo.share_preference.MySharePreference;


import javax.inject.Inject;
import javax.inject.Named;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "ooooo";
    @Inject
    @Named("url")
    String url;

    @Inject
    //@Named("des")
    String des;

    @Inject
    ArticleService articleService;

    @Inject
    MySharePreference mySharePreference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ((MyApp)getApplication()).getMyAppComponent()
                .inject(this);

//        mySharePreference.setPreference("Article Management");
//        Log.e(TAG, "onCreate: "+ mySharePreference.getPreference());
//        Log.e(TAG, "onCreate: "+ url + " , "+ des);




         //********  using retrofit with Call<T> Method
        articleService.getAllArticle().enqueue(new Callback<ArticleResponse>() {
            @Override
            public void onResponse(Call<ArticleResponse> call,
                                   Response<ArticleResponse> response) {
                Log.e(TAG, "onResponse: "+ response.body().getMessage());
            }

            @Override
            public void onFailure(Call<ArticleResponse> call, Throwable t) {
                Log.e(TAG, "onFailure: "+ t.getMessage() );
            }
        });
    }
}
