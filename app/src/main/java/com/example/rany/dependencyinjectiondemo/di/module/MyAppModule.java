package com.example.rany.dependencyinjectiondemo.di.module;


import android.app.Application;
import android.content.Context;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class MyAppModule {

    private Application application;

    public MyAppModule(Application application) {
        this.application = application;
    }

    @Provides
    public Context getApplicationContext(){
        return application.getApplicationContext();
    }

    @Provides
    @Named("url")
    public String provideUrl(){
        return "www.denhtlai.com";
    }

    @Provides
    //@Named("des")
    public String provideDesc(){
        return "Cambodia online auction service";
    }

}
