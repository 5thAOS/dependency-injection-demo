package com.example.rany.dependencyinjectiondemo.di.module;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

@Module
public class NetworkModule {

    @Provides
    public OkHttpClient provideOkHttpClient(){
        final OkHttpClient.Builder okhttpclient = new OkHttpClient.Builder();
        okhttpclient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                Request.Builder builder = original.newBuilder()
                        .header("Accept","application/json");
//                        .header("Authorization", "Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ=");
                Request request = builder.build();
                return chain.proceed(request);
            }
        });

        okhttpclient.connectTimeout(1, TimeUnit.MINUTES)
                .readTimeout(1, TimeUnit.MINUTES)
                .writeTimeout(1, TimeUnit.MINUTES);

        return okhttpclient.build();
    }

}
